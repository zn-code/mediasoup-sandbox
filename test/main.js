
const ortc = require("./ortc");
var mediaCodecs = [
    {
    kind: "audio",
    mimeType: "audio/opus",
    clockRate: 48000,
    channels: 2,
    },
    {
    kind: "video",
    mimeType: "video/VP8",
    clockRate: 90000,
    parameters: {
        //                'x-google-start-bitrate': 1000
    },
    },
    {
    kind: "video",
    mimeType: "video/h264",
    clockRate: 90000,
    parameters: {
        "packetization-mode": 1,
        "profile-level-id": "4d0032",
        "level-asymmetry-allowed": 1,
        //						  'x-google-start-bitrate'  : 1000
    },
    },
    {
    kind: "video",
    mimeType: "video/h264",
    clockRate: 90000,
    parameters: {
        "packetization-mode": 1,
        "profile-level-id": "42e01f",
        "level-asymmetry-allowed": 1,
        //						  'x-google-start-bitrate'  : 1000
    },
    },
]
console.log("mediaCodecs",JSON.stringify(mediaCodecs))
const rtpCapabilities = ortc.generateRouterRtpCapabilities(mediaCodecs)

console.log("rtpCapabilities",JSON.stringify(rtpCapabilities))

const rtpParametersStr = '{"codecs":[{"mimeType":"video/VP8","payloadType":96,"clockRate":90000,"parameters":{},"rtcpFeedback":[{"type":"goog-remb","parameter":""},{"type":"transport-cc","parameter":""},{"type":"ccm","parameter":"fir"},{"type":"nack","parameter":""},{"type":"nack","parameter":"pli"}]},{"mimeType":"video/rtx","payloadType":97,"clockRate":90000,"parameters":{"apt":96},"rtcpFeedback":[]}],"headerExtensions":[{"uri":"urn:ietf:params:rtp-hdrext:sdes:mid","id":4,"encrypt":false,"parameters":{}},{"uri":"urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id","id":5,"encrypt":false,"parameters":{}},{"uri":"urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id","id":6,"encrypt":false,"parameters":{}},{"uri":"http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time","id":2,"encrypt":false,"parameters":{}},{"uri":"http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01","id":3,"encrypt":false,"parameters":{}},{"uri":"urn:3gpp:video-orientation","id":13,"encrypt":false,"parameters":{}},{"uri":"urn:ietf:params:rtp-hdrext:toffset","id":14,"encrypt":false,"parameters":{}}],"encodings":[{"active":true,"scaleResolutionDownBy":4,"maxBitrate":96000,"rid":"r0","scalabilityMode":"S1T3","dtx":false},{"active":true,"scaleResolutionDownBy":1,"maxBitrate":680000,"rid":"r1","scalabilityMode":"S1T3","dtx":false}],"rtcp":{"cname":"","reducedSize":true},"mid":"0"}'
const audiortpParametersStr = '{"codecs":[{"mimeType":"audio/opus","payloadType":111,"clockRate":48000,"channels":2,"parameters":{"minptime":10,"useinbandfec":1},"rtcpFeedback":[{"parameter":"","type":"transport-cc"}]}],"headerExtensions":[{"uri":"urn:ietf:params:rtp-hdrext:sdes:mid","id":4,"encrypt":false,"parameters":{}},{"uri":"http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time","id":2,"encrypt":false,"parameters":{}},{"uri":"http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01","id":3,"encrypt":false,"parameters":{}},{"uri":"urn:ietf:params:rtp-hdrext:ssrc-audio-level","id":1,"encrypt":false,"parameters":{}}],"encodings":[{"ssrc":1971670989,"dtx":false}],"rtcp":{"cname":"0Cn91iZVKluwWvgG","reducedSize":true},"mid":"1"}'; 
var rtpParameters = JSON.parse(rtpParametersStr)
var audiortpParameters = JSON.parse(audiortpParametersStr);
ortc.validateRtpParameters(rtpParameters);
ortc.validateRtpParameters(audiortpParameters);
const rtpMapping = ortc.getProducerRtpParametersMapping(rtpParameters, rtpCapabilities);
const audiortpMapping = ortc.getProducerRtpParametersMapping(audiortpParameters, rtpCapabilities);
console.log("rtpmapping",JSON.stringify(audiortpMapping))
const consumableRtpParameters = ortc.getConsumableRtpParameters("video", rtpParameters, rtpCapabilities, rtpMapping);
const audioconsumableRtpParameters = ortc.getConsumableRtpParameters("audio", audiortpParameters, rtpCapabilities, audiortpMapping);
console.log("consumableRtpParameters",JSON.stringify(audioconsumableRtpParameters))